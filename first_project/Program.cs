﻿using System;

namespace MyProgram
{
    class Program
    {
        static (string, string) InputLoginAndPassword()
        {
            Console.WriteLine("Введите логин: ");
            string login = Console.ReadLine();

            Console.WriteLine("Введите пароль: ");
            string password = Console.ReadLine();

            return (login, password);
        }


        static bool AuthorizeUser()
        {
            bool userAuthorized;
            {
                userAuthorized = false;

                string[] loginList = { "Админ", "Иванова", "Петрова", "Сергеева", "Васильева", "Семёнова" };
                string[] passwordList = { "000", "111", "222", "333", "444", "555" };

                int counterAuthorized = 0;
                const int MAX_AUTHORIZATION_ATTEMPTS = 3;

                bool authorizationAvailable = counterAuthorized < MAX_AUTHORIZATION_ATTEMPTS;


                while (authorizationAvailable)
                {
                    var input = InputLoginAndPassword();
                    string login = input.Item1, password = input.Item2;

                    {
                        for (int index = 0; index < loginList.Length && index < passwordList.Length; index++)
                        {
                            bool loginMatched, passwordMatched;
                            {
                                string loginByCurrentIndex = loginList[index];
                                loginMatched = loginByCurrentIndex == login;
                                string passwordByCurrendIndex = passwordList[index];
                                passwordMatched = passwordByCurrendIndex == password;
                            }

                            if (loginMatched && passwordMatched)
                            {
                                userAuthorized = true;
                                break;
                            }
                        }
                    }

                    if (userAuthorized)
                    {
                        Console.WriteLine("Вы успешно авторизованы!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Логин или пароль введены неправильно!");
                        authorizationAvailable = ++counterAuthorized < MAX_AUTHORIZATION_ATTEMPTS;

                        if (authorizationAvailable)
                        {
                            continue;
                        }
                        else
                        {
                            Console.WriteLine("Вы исчерпали все попытки авторизации. Обратитесь к администратору.");
                            break;
                        }
                    }
                }
            }

            return userAuthorized;
        }


        static string CountryCode()
        {
            Console.WriteLine(@"
                Азербайджан === (994)
                    Армения === (374)
                   Беларусь === (375)
                     Грузия === (995)
                   Киргизия === (996)
                  Казахстан === (997)
                     Латвия === (371)
                      Литва === (370)
                    Молдова === (373)
                     Россия === (007)
                Таджикистан === (992)
                  Туркмения === (993)
                 Узбекистан === (998)
                    Украина === (380)
                    Эстония === (372)");

            bool countryCodeIsCorrect;
            string codeOfTheCountry;

            do
            {
                const string CODE_OF_AZERBAIJAN = "994",
                             CODE_OF_ARMENIA = "374",
                             CODE_OF_BELARUS = "375",
                             CODE_OF_GEORGIA = "995",
                             CODE_OF_KYRGYZSTAN = "996",
                             CODE_OF_KAZAKHSTAN = "997",
                             CODE_OF_LATVIA = "371",
                             CODE_OF_LITHUANIA = "370",
                             CODE_OF_MOLDOVA = "373",
                             CODE_OF_RUSSIA = "007",
                             CODE_OF_TAJIKISTAN = "992",
                             CODE_OF_TURKMENISTAN = "993",
                             CODE_OF_UZBEKISTAN = "998",
                             CODE_OF_UKRAINE = "380",
                             CODE_OF_ESTONIA = "372";

                Console.WriteLine("Введите код страны: ");
                codeOfTheCountry = Console.ReadLine();

                switch (codeOfTheCountry)
                {
                    case CODE_OF_AZERBAIJAN:
                    case CODE_OF_ARMENIA:
                    case CODE_OF_BELARUS:
                    case CODE_OF_GEORGIA:
                    case CODE_OF_KYRGYZSTAN:
                    case CODE_OF_KAZAKHSTAN:
                    case CODE_OF_LATVIA:
                    case CODE_OF_LITHUANIA:
                    case CODE_OF_MOLDOVA:
                    case CODE_OF_RUSSIA:
                    case CODE_OF_TAJIKISTAN:
                    case CODE_OF_TURKMENISTAN:
                    case CODE_OF_UZBEKISTAN:
                    case CODE_OF_UKRAINE:
                    case CODE_OF_ESTONIA:
                        {
                            countryCodeIsCorrect = true;
                            break;
                        }
                    default:
                        {
                            countryCodeIsCorrect = false;

                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"Неверный код страны {codeOfTheCountry}!");
                            Console.ForegroundColor = ConsoleColor.Gray;

                            break;
                        }
                }
            }
            while (countryCodeIsCorrect == false);

            return codeOfTheCountry;
        }


        static decimal Coefficient()
        {
            const decimal RATE_AZERBAIJAN = 1.07m,
                          RATE_ARMENIA = 0.95m,
                          RATE_BELARUS = 1m,
                          RATE_GEORGIA = 0.94m,
                          RATE_KYRGYZSTAN = 0.83m,
                          RATE_KAZAKHSTAN = 0.79m,
                          RATE_LATVIA = 1.12m,
                          RATE_LITHUANIA = 1.12m,
                          RATE_MOLDOVA = 0.97m,
                          RATE_RUSSIA = 1m,
                          RATE_TAJIKISTAN = 0.76m,
                          RATE_TURKMENISTAN = 0.81m,
                          RATE_UZBEKISTAN = 0.98m,
                          RATE_UKRAINE = 1m,
                          RATE_ESTONIA = 1.12m;

            const string  CODE_OF_AZERBAIJAN = "994",
                          CODE_OF_ARMENIA = "374",
                          CODE_OF_BELARUS = "375",
                          CODE_OF_GEORGIA = "995",
                          CODE_OF_KYRGYZSTAN = "996",
                          CODE_OF_KAZAKHSTAN = "997",
                          CODE_OF_LATVIA = "371",
                          CODE_OF_LITHUANIA = "370",
                          CODE_OF_MOLDOVA = "373",
                          CODE_OF_RUSSIA = "007",
                          CODE_OF_TAJIKISTAN = "992",
                          CODE_OF_TURKMENISTAN = "993",
                          CODE_OF_UZBEKISTAN = "998",
                          CODE_OF_UKRAINE = "380",
                          CODE_OF_ESTONIA = "372";

            string codeOfTheCountry = CountryCode();

            decimal multiplierCoefficient;

            switch (codeOfTheCountry)
            {
                case CODE_OF_AZERBAIJAN:
                    {
                        multiplierCoefficient = RATE_AZERBAIJAN;
                        break;
                    }
                case CODE_OF_ARMENIA:
                    {
                        multiplierCoefficient = RATE_ARMENIA;
                        break;
                    }
                case CODE_OF_BELARUS:
                    {
                        multiplierCoefficient = RATE_BELARUS;
                        break;
                    }
                case CODE_OF_GEORGIA:
                    {
                        multiplierCoefficient = RATE_GEORGIA;
                        break;
                    }
                case CODE_OF_KYRGYZSTAN:
                    {
                        multiplierCoefficient = RATE_KYRGYZSTAN;
                        break;
                    }
                case CODE_OF_KAZAKHSTAN:
                    {
                        multiplierCoefficient = RATE_KAZAKHSTAN;
                        break;
                    }
                case CODE_OF_LATVIA:
                    {
                        multiplierCoefficient = RATE_LATVIA;
                        break;
                    }
                case CODE_OF_LITHUANIA:
                    {
                        multiplierCoefficient = RATE_LITHUANIA;
                        break;
                    }
                case CODE_OF_MOLDOVA:
                    {
                        multiplierCoefficient = RATE_MOLDOVA;
                        break;
                    }
                case CODE_OF_RUSSIA:
                    {
                        multiplierCoefficient = RATE_RUSSIA;
                        break;
                    }
                case CODE_OF_TAJIKISTAN:
                    {
                        multiplierCoefficient = RATE_TAJIKISTAN;
                        break;
                    }
                case CODE_OF_TURKMENISTAN:
                    {
                        multiplierCoefficient = RATE_TURKMENISTAN;
                        break;
                    }
                case CODE_OF_UZBEKISTAN:
                    {
                        multiplierCoefficient = RATE_UZBEKISTAN;
                        break;
                    }
                case CODE_OF_UKRAINE:
                    {
                        multiplierCoefficient = RATE_UKRAINE;
                        break;
                    }
                case CODE_OF_ESTONIA:
                    {
                        multiplierCoefficient = RATE_ESTONIA;
                        break;
                    }
                default:
                    {
                        multiplierCoefficient = 1;
                        break;
                    }
            }

            return multiplierCoefficient;
        }


        static void Main(string[] args)
        {
            bool userAuthorized = AuthorizeUser();

            while (userAuthorized)
            {
                decimal multiplierCoefficient = Coefficient();

                decimal tile, costOfTile, totalCostOfTile;
                {
                    Console.WriteLine("Введите количество плитки в м2: ");
                    tile = Convert.ToInt64(Console.ReadLine());

                    Console.WriteLine("Введите стоимость плитки за м2 в рублях: ");
                    costOfTile = Convert.ToInt64(Console.ReadLine());

                    totalCostOfTile = checked((tile * costOfTile) * multiplierCoefficient);
                }

                decimal discount;
                {
                    bool discount20Available, discount50Available;
                    {
                        const decimal TILE_FOR_DISCOUNT_20 = 500,
                                      TILE_FOR_DISCOUNT_50 = 1000;

                        discount20Available = tile >= TILE_FOR_DISCOUNT_20 &&
                                              tile <= TILE_FOR_DISCOUNT_50;

                        discount50Available = tile >= TILE_FOR_DISCOUNT_50;
                    }

                    if (discount20Available)
                    {
                        const decimal MIN_DISCOUNT = 20; // процентов
                        discount = totalCostOfTile * MIN_DISCOUNT / 100;
                    }
                    else if (discount50Available)
                    {
                        const decimal MAX_DISCOUNT = 50; // процентов
                        discount = totalCostOfTile * MAX_DISCOUNT / 100;
                    }
                    else
                    {
                        discount = 0;
                    }
                }

                decimal finalCost = totalCostOfTile - discount;

                Console.WriteLine($"Общая стоимость плитки будет равна : {totalCostOfTile} рублей");
                Console.WriteLine($"Сумма скидки будет равна           : {discount} рублей");
                Console.WriteLine($"Итоговая стоимость с учетом скидки : {finalCost} рублей");
                Console.WriteLine($"Нажмите клавишу ENTER для начала обслуживания следующего клиента");
                Console.WriteLine("=================================================================");

                Console.ReadKey();
            }
        }
    }
}