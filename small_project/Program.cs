﻿using System;

namespace MyProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfTypesOfTile, numberOfTypesOfMaterials;
            {
                Console.WriteLine("Введите количество видов плитки: ");
                numberOfTypesOfTile = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Введите количество видов материала: ");
                numberOfTypesOfMaterials = Convert.ToInt32(Console.ReadLine());
            }

            decimal[,] rawMaterial = new decimal[numberOfTypesOfTile, numberOfTypesOfMaterials];
            decimal[,] priceMaterial = new decimal[numberOfTypesOfMaterials, 1];
            decimal[] plannedVolumeOfTiles = new decimal[numberOfTypesOfTile];

            decimal[,] costOfMaterial = new decimal[numberOfTypesOfTile, 1];

            for (int i = 0; i < rawMaterial.GetLength(0); i++)
                for (int j = 0; j < rawMaterial.GetLength(1); j++)
                {
                    Console.WriteLine($"Введите количество сырья {j + 1} для плитки {i + 1}");
                    rawMaterial[i, j] = Convert.ToDecimal(Console.ReadLine());
                }


            for (int i = 0; i < priceMaterial.GetLength(0); i++)
            {
                Console.WriteLine($"Введите цену на материалы {i + 1}");
                priceMaterial[i, 0] = Convert.ToDecimal(Console.ReadLine());
            }


            for (int i = 0; i < plannedVolumeOfTiles.GetLength(0); i++)
            {
                Console.WriteLine($"Введите планируемый объем выпуска плитки {i + 1}");
                plannedVolumeOfTiles[i] = Convert.ToDecimal(Console.ReadLine());
            }


            for (int i = 0; i < rawMaterial.GetLength(0); i++)
            {
                for (int j = 0; j < rawMaterial.GetLength(1); j++)
                {
                    costOfMaterial[i, 0] += rawMaterial[i, j] * priceMaterial[j, 0];
                }
            }

            decimal totalCost = 0;

            for (int i = 0; i < costOfMaterial.GetLength(0); i++)
            {
                totalCost += plannedVolumeOfTiles[i] * costOfMaterial[i, 0];
            }

            Console.WriteLine($"Общая стоимость сырья составляет: {totalCost} рублей");

            Console.ReadKey();
        }
    }
}